package main

import (
	"bytes"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"golang.org/x/crypto/argon2"
	"time"
)

// Types
type users struct {
	UserHash     []byte
	EmailHash    []byte
	TokenHash    []byte
	TokenTime    time.Time
	FastHashAlgo int
	PassHash     []byte
	PassSalt     []byte
	PassHashAlgo int
	PublicKey    []byte
	PrivateEnc   string
}

type actionAuth struct {
	User string
	Pass string
}

type actionAdd struct {
	User       string
	Email      string
	Pass       string
	PassHash   []byte
	Salt       []byte
	Public     rsa.PublicKey
	PrivateEnc string
}

// Errors
type userExistsErr struct {
	user string
}

func (e userExistsErr) Error() string {
	return fmt.Sprintf("%s already exists", e.user)
}

type userDoesNotExistErr struct {
	user string
}

func (e userDoesNotExistErr) Error() string {
	return fmt.Sprintf("%s does not exist", e.user)
}

type hashesDoNotMatchErr struct {
	gotHash      []byte
	expectedHash []byte
}

func (e hashesDoNotMatchErr) Error() string {
	return fmt.Sprintf("got has %s but expected %s", e.gotHash, e.expectedHash)
}

// Constants
const ( // Password hash algorithms
	argon2idHash = iota
)
const PreferredPassHash = argon2idHash // PreferredPassHash hash algorithm

const ( // Fast hash algorithms
	sha256Hash = iota
)
const PreferredFastHash = sha256Hash

// Functions
func hashPass(passRaw string, salt []byte, algorithm int) []byte {
	switch algorithm {
	case argon2idHash:
		fallthrough
	default:
		return argon2.IDKey([]byte(passRaw), salt, 1, 64*1024, 4, 32)
	}
}

func hashFast(message []byte, algorithm int) []byte {
	switch algorithm {
	case sha256Hash:
		fallthrough
	default:
		hash := sha256.Sum256(message)
		return hash[:]
	}
}

func encodePublic(publicKey rsa.PublicKey) []byte {
	encoded, err := json.Marshal(publicKey)
	if err != nil {
		panic(err) // TODO: Error handling
	}
	return encoded
}

func decodePublic(encoded []byte) rsa.PublicKey {
	var publicKey rsa.PublicKey
	json.Unmarshal(encoded, &publicKey)
	return publicKey
}

func create(u actionAdd) (string, error) {
	userHash := hashFast([]byte(u.User), sha256Hash)
	if exists(userHash) { // True if user exists
		return "",
			userExistsErr{
				user: u.User,
			}
	}

	l.Println("in actionAdd")

	emailHash := hashFast([]byte(u.Email), PreferredFastHash)

	passRaw := u.Pass
	passSalt := make([]byte, 32)
	rand.Read(passSalt)

	passHash := hashPass(passRaw, passSalt, PreferredPassHash)
	l.Println(passHash)

	token := make([]byte, 32) // TODO: Code reduplication
	rand.Read(token)
	tokenHash := hashFast(token, PreferredFastHash)

	l.Println("Adding user")
	addUser(users{
		UserHash:     userHash,
		EmailHash:    emailHash,
		TokenHash:    tokenHash,
		FastHashAlgo: PreferredFastHash,
		PassHash:     passHash,
		PassSalt:     passSalt,
		PassHashAlgo: PreferredPassHash,
		PublicKey:    encodePublic(u.Public),
		PrivateEnc:   u.PrivateEnc,
	})
	l.Printf("Successfully added user %s\n", u.User)

	return authorize(actionAuth{
		User: u.User,
		Pass: u.Pass,
	})
}

func authorize(u actionAuth) (string, error) {
	userHash := sha256.Sum256([]byte(u.User))
	if !exists(userHash[:]) { // True if user does not exist
		return "",
			userDoesNotExistErr{
				user: u.User,
			}
	}

	data := getUser(userHash[:])
	salt := data.PassSalt
	passRaw := u.Pass
	l.Println(salt)

	gotHash := hashPass(passRaw, salt, data.PassHashAlgo)
	expectedHash := getUser(userHash[:]).PassHash

	if !bytes.Equal(gotHash, expectedHash) {
		return "",
			hashesDoNotMatchErr{
				gotHash:      gotHash,
				expectedHash: expectedHash,
			}
	}

	if data.PassHashAlgo != PreferredPassHash { // Update to preferred hash algorithm
		l.Println("Updating hash algorithm")
		salt = make([]byte, 32)
		rand.Read(salt)

		hash := hashPass(passRaw, salt, PreferredPassHash)

		data.PassHash = hash
		data.PassSalt = salt
		data.PassHashAlgo = PreferredPassHash
	}

	token := make([]byte, 32)
	rand.Read(token)

	tokenHash := sha256.Sum256(token)
	data.TokenHash = tokenHash[:]

	updateUser(data)

	publicKey := decodePublic(data.PublicKey)
	encryptedToken, err := rsa.EncryptOAEP(sha256.New(), rand.Reader, &publicKey, token, []byte(""))
	if err != nil {
		panic(err) // TODO: Error handling
	}

	stringToken := base64.StdEncoding.EncodeToString(encryptedToken)

	l.Printf("Successfully authenticated user %s\n", u.User)
	return stringToken, nil
}
