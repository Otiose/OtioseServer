// Runner for OtioseServer
// Run with `go run *.go`
package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"os"
)

// Types
type configServer struct {
	Port string
}

type newRequest struct {
	Action string
}

// Constants
const configServerFile = "configServer.json"

// Logging
var l *log.Logger

func init() {
	file, err := os.OpenFile("main.log", os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0666)
	if err != nil {
		panic(err)
	}
	l = log.New(file, "", log.Ldate|log.Ltime|log.Llongfile)
	l.Println("Session started")
}

// Handling
func handle(conn net.Conn) {
	l.Println("New connection")
	defer conn.Close()
	var buf bytes.Buffer
	io.Copy(&buf, conn)
	l.Println(buf.String())

	var request newRequest
	json.Unmarshal(buf.Bytes(), &request)
	switch request.Action {
	case "auth":
		var auth actionAuth
		json.Unmarshal(buf.Bytes(), &auth)
		stringToken, err := authorize(auth)
		if err != nil {
			l.Println(err)
			fmt.Fprint(conn, `{"err": "incorrect username or password"}`)
			break
		}

		fmt.Fprintln(conn, fmt.Sprintf(`{"token": "%s"}`, stringToken))
		l.Println("Success!")
	case "add":
		var add actionAdd
		json.Unmarshal(buf.Bytes(), &add)
		stringToken, err := create(add)
		if err != nil {
			l.Println(err)
			fmt.Fprint(conn, fmt.Sprintf(`{"err": "user already exists"}`))
			break
		}

		fmt.Fprintln(conn, fmt.Sprintf(`{"token": "%s"}`, stringToken))
	default:
		fmt.Fprint(conn, `{"err": "unknown operation"}`)
	}
}

func main() {
	defer closeDatabase()
	data, err := ioutil.ReadFile(configServerFile)
	if err != nil {
		l.Fatalln(err)
	}

	var config configServer
	err = json.Unmarshal(data, &config)
	if err != nil {
		l.Fatalln(err)
	}

	listener, err := net.Listen("tcp", fmt.Sprintf(":%s", config.Port))
	if err != nil {
		l.Fatalln(err)
	}

	for {
		conn, err := listener.Accept()
		if err != nil {
			panic(err)
		}

		go handle(conn)
	}
}
