package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	_ "github.com/lib/pq"
	"io/ioutil"
	"time"
)

// Types
type configDB struct {
	Host     string
	Port     string
	User     string
	Password string
	Name     string
}

// Constants
const configDBFile = "configDB.json"

// Globals
var db *sql.DB

// Initialize db
func init() {
	data, err := ioutil.ReadFile(configDBFile)
	if err != nil {
		panic(err)
	}

	var config configDB
	err = json.Unmarshal(data, &config)
	if err != nil {
		panic(err)
	}

	dbInfo := fmt.Sprintf(
		"host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		config.Host, config.Port, config.User, config.Password, config.Name,
	)

	db, err = sql.Open("postgres", dbInfo)
	if err != nil {
		panic(err)
	}

	const createTables = `
		CREATE TABLE users (
			user_hash bytea PRIMARY KEY, -- Hash of the user's username using sha256
			email_hash bytea, -- Hash of the user's password using fast_hash_algo
			token_hash bytea, -- Hash of the user's token using fast_hash_algo
			token_time timestamp with time zone DEFAULT clock_timestamp(), -- The time the token was added to the system
			fast_hash_algo int, -- Hash algorithm used for email_hash and token_hash
			pass_hash bytea, -- Hash of the user's password using pass_hash_algo
			pass_salt bytea, -- Salt used for pass_hash
			pass_hash_algo int, -- Hash algorithm used with pass_salt for pass_hash
			public_key bytea, -- The user's public key
			private_enc text -- An encrypted version of the user's private key
		);
		CREATE TABLE groups (
			group_id bigserial PRIMARY KEY, -- The id of the group
			name_enc bytea -- Group name encrypted symmetrically with group password
		);
		CREATE TABLE group_data (
			group_id bigint, -- The id of the group
			user_hash bytea, -- Hash of the user's username using sha256
			pass_enc bytea -- Group password encrypted using the user's public key
		);
		`

	db.Exec(createTables)
}

// Functions
func closeDatabase() {
	db.Close()
}

func exists(userHash []byte) bool { // TODO: Error Handling
	const query = `SELECT EXISTS(SELECT 1 FROM users WHERE user_hash = $1 LIMIT 1)`
	rows, err := db.Query(query, userHash)
	if err != nil {
		l.Fatalln(err)
	}
	defer rows.Close()
	rows.Next()

	var exists bool
	err = rows.Scan(&exists)
	if err != nil {
		l.Fatalln(err)
	}
	return exists
}

func getUser(userHash []byte) users { // TODO: Error Handling
	const query = `SELECT 
		email_hash,
		token_hash,
		token_time,
		fast_hash_algo,
		pass_hash,
		pass_salt,
		pass_hash_algo,
		public_key,
		private_enc
	FROM users WHERE user_hash = $1 LIMIT 1`
	rows, err := db.Query(query, userHash)
	if err != nil {
		l.Fatalln(err)
	}
	defer rows.Close()
	rows.Next()

	var emailHash []byte
	var tokenHash []byte
	var tokenTime time.Time
	var fastHashAlgo int
	var passHash []byte
	var passSalt []byte
	var passHashAlgo int
	var publicKey []byte
	var privateEnc string
	err = rows.Scan(&emailHash, &tokenHash, &tokenTime, &fastHashAlgo, &passHash, &passSalt, &passHashAlgo, &publicKey, &privateEnc)
	if err != nil {
		l.Fatalln(err)
	}

	return users{
		UserHash:     userHash,
		EmailHash:    emailHash,
		TokenHash:    tokenHash,
		TokenTime:    tokenTime,
		FastHashAlgo: fastHashAlgo,
		PassHash:     passHash,
		PassSalt:     passSalt,
		PassHashAlgo: passHashAlgo,
		PublicKey:    publicKey,
		PrivateEnc:   privateEnc,
	}
}

func addUser(data users) { // TODO: Error Handling
	const query = `INSERT INTO users (
		user_hash,
		email_hash,
		token_hash,
		fast_hash_algo,
		pass_hash,
		pass_salt,
		pass_hash_algo,
		public_key,
		private_enc
	) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)`
	stmt, err := db.Prepare(query)
	if err != nil {
		l.Fatalln(err)
	}

	_, err = stmt.Exec(
		data.UserHash,
		data.EmailHash,
		data.TokenHash,
		data.FastHashAlgo,
		data.PassHash,
		data.PassSalt,
		data.PassHashAlgo,
		data.PublicKey,
		data.PrivateEnc,
	)
	if err != nil {
		l.Fatalln(err)
	}
}

func updateUser(data users) { // TODO: Error Handling
	const query = `UPDATE users SET (
		user_hash,
		email_hash,
		token_hash,
		token_time,
		fast_hash_algo,
		pass_hash,
		pass_salt,
		pass_hash_algo,
		public_key,
		private_enc
	) = ($1, $2, $3, clock_timestamp(), $4, $5, $6, $7, $8, $9) WHERE user_hash = $1`
	stmt, err := db.Prepare(query)
	if err != nil {
		l.Fatalln(err)
	}

	_, err = stmt.Exec(
		data.UserHash,
		data.EmailHash,
		data.TokenHash,
		data.FastHashAlgo,
		data.PassHash,
		data.PassSalt,
		data.PassHashAlgo,
		data.PublicKey,
		data.PrivateEnc,
	)
	if err != nil {
		l.Fatalln(err)
	}
}
